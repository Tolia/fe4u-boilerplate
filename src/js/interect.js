function addingFormAppear() {
  document.getElementById('add_teacher_pop_up').style.visibility = 'visible';
  document.getElementById('content').style.filter = 'blur(3px)';
}

function closeAddingForm() {
  document.getElementById('add_teacher_pop_up').style.visibility = 'hidden';
  document.getElementById('content').style.filter = 'none';
}

function infoOpen(img) {
  document.getElementById('teach_info').style.visibility = 'visible';
  document.getElementById('content').style.filter = 'blur(3px)';
  const el = document.querySelector('#teach_info main');
  if (img.src !== '') {
    el.innerHTML = `<img src=${img.src}>`;
  } else {
    el.innerHTML = '<img src=\'..\\images\\avatar.png\'>';
  }
  el.innerHTML += `<section><h1>${img.dataset.fullName}</h1>`
  + `<h2>${img.dataset.prof}</h2>`
  + `<h3>${img.dataset.city}, ${img.dataset.country}</h3>`
  + `<h3>${img.dataset.age}, ${img.dataset.gender}</h3>`
  + ` <h3>${img.dataset.email}</h3>`
  + `<h3>${img.dataset.phone}</h3>`
  + '</section>';
}

function closeInfo() {
  document.getElementById('teach_info').style.visibility = 'hidden';
  document.getElementById('content').style.filter = 'none';
}

document.querySelectorAll('#content > header > button, #content > footer > button ').forEach((button) => button.addEventListener('click', () => addingFormAppear()));
document.querySelectorAll('.teach_list img').forEach((img) => img.addEventListener('click', () => infoOpen(img)));
document.querySelector('#add_teacher_pop_up > header > img').addEventListener('click', () => closeAddingForm());
document.querySelector('#teach_info > header> img').addEventListener('click', () => closeInfo());
