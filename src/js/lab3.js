import { randomUserMock, additionalUsers } from './FE4U-Lab3-mock.js';
import { countriesCodes } from './countries-codes.js';
import { parsePhoneNumber } from '../../node_modules/libphonenumber-js/index.js';

const nats = new Map();

countriesCodes.forEach(({ name, code }) => nats.set(name, code));

export function task1() {
  const formattedRandomUserMock = randomUserMock.map((teacher) => ({
    gender: teacher.gender,
    title: teacher.name.title,
    full_name: `${teacher.name.first} ${teacher.name.last}`,
    city: teacher.location.city,
    state: teacher.location.state,
    country: teacher.location.country,
    postcode: teacher.location.postcode,
    coordinates: teacher.location.coordinates,
    timezone: teacher.location.timezone,
    email: teacher.email,
    b_date: teacher.dob.date,
    age: teacher.dob.age,
    phone: teacher.phone,
    picture_Large: teacher.picture.large,
    picture_tumbnail: teacher.picture.thumbnail,
  }));
  let counter = 1;
  const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry',
    'Law', 'Art', 'Medicine', 'Statistics'];
  const formattedRandomUserMockWithAdditionalFields = new Array(randomUserMock.length);
  for (let i = 0; i < formattedRandomUserMock.length; i += 1) {
    const teacher = formattedRandomUserMock[i];
    formattedRandomUserMockWithAdditionalFields[i] = {
      id: randomUserMock[i].id.name === '' ? (counter).toString() : randomUserMock[i].id.name + randomUserMock[i].id.value,
      favorite: Boolean(Math.round(Math.random())),
      course: courses[Math.round(Math.random() * (courses.length - 1))],
      ...teacher,
      bg_color: `#${Math.round(Math.random() * 0xffffff).toString(16)}`,
    };
    counter += 1;
  }
  for (let i = 0; i < additionalUsers.length; i += 1) {
    if (additionalUsers[i].course == null) {
      additionalUsers[i].course = courses[Math.round(Math.random() * (courses.length - 1))];
    }
  }
  const mergedRes = formattedRandomUserMockWithAdditionalFields
    .filter((teacher) => !additionalUsers.some(({ id }) => id === teacher.id));
  return mergedRes.concat(additionalUsers);
}

export function task2(element) {
  const checkIfStringHelpArr = [element.full_name, element.gender, element.note,
    element.state, element.city, element.country];
  if (!checkIfStringHelpArr.filter((el) => el !== undefined).every((el) => typeof el === 'string' && el.charAt(0) === el.charAt(0).toUpperCase())) {
    return false;
  }
  if (typeof element.age !== 'number') {
    return false;
  }
  const reg = /.*@.*/;
  if (!reg.test(element.email)) {
    return false;
  }
  try {
    if (!parsePhoneNumber(element.phone, nats.get(element.country)).isValid()) {
      return false;
    }
  } catch (ex) {
    return false;
  }
  return true;
}

export function task3(arrayOfObjs, country, age, gender, favorite) {
  return arrayOfObjs.filter((el) => el.country === country
    && el.age === age && el.gender === gender && el.favorite === favorite);
}

export function task4(arrayOfObjs, key, asceding) {
  const multip = asceding ? 1 : -1;
  arrayOfObjs.sort((a, b) => {
    if (a[key] < b[key]) {
      return -1 * multip;
    }
    return a[key] === b[key] ? 0 : multip;
  });
  return arrayOfObjs;
}

export function findTeachers(arrayOfObjs, parameter) {
  return arrayOfObjs.filter((el) => Object.values(el).some((value) => value === parameter));
}

export function task5(arrayOfObjs, parameter) {
  return findTeachers(arrayOfObjs, parameter)[0];
}

export function task6(arrayOfObjs, parameter) {
  return Math.round((findTeachers(arrayOfObjs, parameter).length * 100) / arrayOfObjs.length) / 100;
}

let flag = false;
document.querySelector('#task1 button').addEventListener('click', () => {
  document.querySelector('#task1 main').innerHTML = '';
  document.querySelector('#task1 button').textContent = 'Показати відповідь';
  if (!flag) {
    const res = task1();
    for (let i = 0; i < res.length; i += 1) {
      const el = res[i];
      document.querySelector('#task1 main').innerHTML = `${document.querySelector('#task1 main').innerHTML}
      ${JSON.stringify(JSON.stringify(el))}<br>`;
    }
    document.querySelector('#task1 button').textContent = 'Сховати відповідь';
    flag = true;
  } else {
    flag = false;
  }
});

document.querySelector('#task2 button').addEventListener('click', () => {
  const el = JSON.parse(document.querySelector('#task2 textarea').value);
  console.log(el);
  if (task2(el)) {
    alert('Проходить валідацію');
  } else {
    alert('Не проходить валідацію');
  }
});

document.querySelector('#task3 button').addEventListener('click', () => {
  const arr = JSON.parse(document.querySelector('#task3 textarea').value);
  const country = document.querySelector('#task3 input[name=country]').value;
  const age = parseInt(document.querySelector('#task3 input[name=age]').value, 10);
  const gender = document.querySelector('#task3 select[name=gender]').value;
  const fav = document.querySelector('#task3 input[name=fav]').checked;
  document.querySelector('#task3 main').innerHTML = JSON.stringify(task3(arr, country, age, gender, fav));
});

document.querySelector('#task4 button').addEventListener('click', () => {
  const arr = JSON.parse(document.querySelector('#task4 textarea').value);
  const field = document.querySelector('#task4 input[name=field]').value;
  const asc = !document.querySelector('#task4 input[name=asc]').checked;
  document.querySelector('#task4 main').innerHTML = JSON.stringify(task4(arr, field, asc));
});

document.querySelector('#task5 button').addEventListener('click', () => {
  const arr = JSON.parse(document.querySelector('#task5 textarea').value);
  const field = document.querySelector('#task5 input[name=field]').value;
  document.querySelector('#task5 main').innerHTML = JSON.stringify(task5(arr, field));
});

document.querySelector('#task6 button').addEventListener('click', () => {
  const arr = JSON.parse(document.querySelector('#task6 textarea').value);
  const field = document.querySelector('#task6 input[name=field]').value;
  document.querySelector('#task6 main').innerHTML = JSON.stringify(task6(arr, field));
});
